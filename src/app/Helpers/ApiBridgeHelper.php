<?php

namespace Artlook\Frontend\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log as Log;
use GuzzleHttp\Client;

class ApiBridgeHelper
{
    private static $apiCallsCache = array();

    /**
     * @param Request $request
     * @param $endpoint
     * @return mixed
     */
    public function getData(Request $request, $endpoint, $parameters = null)
    {

        $artlookEndpoint = config($endpoint);

        if (is_null($artlookEndpoint)) {
            abort(500);
        }

        $parameterBag = $parameters;

        $queryData = $this->getQueryData($request, $endpoint, $parameters);
        $artlookEndpointPath = $artlookEndpoint['path'];

        preg_match_all('/({(.*?)})/', $artlookEndpointPath, $artlookEndpointPathKeys);

        if ($artlookEndpointPathKeys !== false) {
            foreach ($artlookEndpointPathKeys[2] as $matchIndex => $matchKey) {
                if (array_key_exists($matchKey, $parameterBag)) {
                    preg_match('/{' . $matchKey . '}/', $artlookEndpointPath, $q);
                    $artlookEndpointPath = preg_replace('/{' . $matchKey . '}/', $parameterBag[$matchKey], $artlookEndpointPath);
                    unset($parameterBag[$matchKey]);
                }
            }
        }

        $queryData = array_merge(is_array($queryData) ? $queryData : array(), $parameterBag);

        $requestUri = config('artlook.api.url') . $artlookEndpointPath . (!empty($queryData) ? ('?' . http_build_query($queryData)) : '');

//        Log::info('debug', ['requestUri' => $requestUri]);

//THIS IS WHERE THE API IS CALLED FOR DATA :-

//        if(str_contains($requestUri, 'exhib'))
//            dd($requestUri);

        if (isset(self::$apiCallsCache[md5($requestUri)])) {
            if (env('APP_DEBUG_INTO')) {
                Log::info(__CLASS__ . '\\' . __FUNCTION__ . ' CACHED:' . config('artlook.api.url') . $artlookEndpointPath . (!empty($queryData) ? ('?' . http_build_query($queryData)) : ''));
            }
            return self::$apiCallsCache[md5($requestUri)];
        }

        $client = new Client();

        try {
            $response = $client->request('GET', $requestUri, [
                'verify' => false
            ]);
        } catch (\Exception $e) {
            abort($e->getCode());
        }

//if(str_contains($requestUri, 'groups'))
//    dd(json_decode($response->getBody()));

        if (env('APP_DEBUG_INTO')) {
            Log::info(__CLASS__ . '\\' . __FUNCTION__ . ' GET:' . config('artlook.api.url') . $artlookEndpointPath . (!empty($queryData) ? ('?' . http_build_query($queryData)) : ''));
        }

        if (preg_match('/^(4|5)\d{2}$/is', $response->getStatusCode())) {
            abort($response->getStatusCode());
        }

        $body = $response->getBody();
        $data = json_decode($body);

        $returnedData = (!isset($data->data) || empty($data->data)) ? null : $data->data;

        self::$apiCallsCache[md5($requestUri)] = $returnedData;

        if (env('APP_DEBUG_INTO')) {
            Log::info(__CLASS__ . '\\' . __FUNCTION__ . ' | data: ' . print_r($returnedData, true));
        }

        return $returnedData;
    }

    /**
     * @param Request $request
     * @param $endpoint
     * @return mixed
     */
    protected function getQueryData(Request $request, $endpoint, $parameters = null)
    {
        $artlookEndpoint = config($endpoint);

        if (!isset($artlookEndpoint['query_string']))
            return null;

        $queryData = $artlookEndpoint['query_string'];

        if (isset($queryData['tenant_id'])) {
            $queryData = array_merge($queryData, [
                'tenant_id' => $request->attributes->get('tenant_id')
            ]);
        }

        if (isset($queryData['api_code'])) {
            $queryData = array_merge($queryData, [
                'api_code' => $request->attributes->get('api_code')
            ]);
        }

        if (!is_null($parameters)
            && is_array($parameters)
            && !empty($parameters)
        ) {
            $queryData = array_merge($queryData, $parameters);
        }

        return is_array($queryData) && !empty($queryData) ? $queryData : null;
    }
}
