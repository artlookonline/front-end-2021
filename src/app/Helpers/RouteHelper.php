<?php

namespace Artlook\Frontend\Helpers;

use Illuminate\Http\Request;

class RouteHelper
{
    public function route($name, $parameters = array(), $absolute = true)
    {
        $domainType = app('request')->attributes->get('domain_type');
        if ($domainType === 'domain') {
            unset($parameters['user_space']);
        }
        try {
            return app('url')->route($domainType . '::' . $name, $parameters, $absolute);
        } catch (\Exception $e) {
            dd($e);
        }
    }
}