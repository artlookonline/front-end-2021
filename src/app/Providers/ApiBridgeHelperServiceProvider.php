<?php

namespace Artlook\Frontend\Providers;

use Illuminate\Support\ServiceProvider;
use Artlook\Frontend\Helpers\ApiBridgeHelper;

class ApiBridgeHelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('helper.api_bridge', function ($app) {
            return new ApiBridgeHelper();
        });
    }
}
