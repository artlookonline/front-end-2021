<?php

namespace Artlook\Frontend\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Artlook\Frontend\Helpers\ViewHelper;

/**
 * ToDo
 *
 * ZCL@20170828
 *
 * Cambiar el uso de los
 * helpers por los facades
 * de los mismos para ViewHelper
 * Route y routeHelper.
 */
/**
 * Class ViewController
 * @package Artlook\Frontend\Http\Controllers
 */

class ViewController extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    /**
     * @param Request $request
     * @param array $parameters
     * @return $this
     */
    public function getMainMenu(Request $request, array $parameters = [])
    {
        $settings = $request->attributes->get('settings');

        $menu = $settings->menu;
        $currentMenuItem = null;

        $route = Route::current();
        $routeFullname = $route->getName();
        preg_match('/::(.*)$/is', $routeFullname, $routeFullnameSegments);
        $routeName = array_last($routeFullnameSegments);

        $routeHelper = app()->make('helper.route');

        if (preg_match('/\_(item|(page|gallery|exhibition)\_artist)$/is', $routeName)) {
            if ($routeName === 'user_space_item') {
                $currentMenuItem = 'home';
            } elseif ($routeName === 'user_space_exhibition_item'
                || preg_match('/\_exhibition\_artist(\_item)?$/is', $routeName)) {
                foreach ($menu as $menuItem) {
                    if (current($menuItem) === 'exhibitions') {
                        $currentMenuItem = current($menuItem);
                        break;
                    }
                }
            } elseif ($routeName === 'user_space_gallery_item'
                || preg_match('/\_gallery\_artist(\_item)?$/is', $routeName)) {
                foreach ($menu as $menuItem) {
                    if (current($menuItem) === 'galleries') {
                        $currentMenuItem = current($menuItem);
                        break;
                    }
                }
            } elseif ($routeName === 'user_space_artist_item') {
                foreach ($menu as $menuItem) {
                    if (current($menuItem) === 'artists') {
                        $currentMenuItem = current($menuItem);
                        break;
                    }
                }
            } elseif ($routeName === 'user_space_pages_item'
                || preg_match('/\_page\_artist(\_item)?$/is', $routeName)) {
                if (isset($request->user_space)
                    && isset($request->page_id)
                ) {
                    $pageSlug = ($request->attributes->has('page_slug')) ? $request->attributes->get('page_slug') : uniqid();
                    $ancestorPath = $routeHelper->route('user_space_page', ['user_space' => $request->user_space, 'id' => $request->page_id, 'slug' => $pageSlug], false);
                    foreach ($menu as $menuItem) {
                        if ($request->attributes->get('use_user_type_for_the_route')) {
                            if ('/' . $request->user_space . '/' . current($menuItem) === $ancestorPath) {
                                $currentMenuItem = current($menuItem);
                                break;
                            }
                        } else {
                            if ('/' . current($menuItem) === $ancestorPath) {
                                $currentMenuItem = current($menuItem);
                                break;
                            }
                        }
                    }
                }
            }
        } elseif ($routeName === 'user_space_artist') {
            foreach ($menu as $menuItem) {
                if (current($menuItem) === 'artists') {
                    $currentMenuItem = current($menuItem);
                    break;
                }
            }
        } elseif ($routeName === 'user_space_exhibition') {
            foreach ($menu as $menuItem) {
                if (current($menuItem) === 'exhibitions') {
                    $currentMenuItem = current($menuItem);
                    break;
                }
            }
        } elseif ($routeName === 'user_space_gallery') {
            foreach ($menu as $menuItem) {
                if (current($menuItem) === 'galleries') {
                    $currentMenuItem = current($menuItem);
                    break;
                }
            }
        } else {
            foreach ($menu as $menuItem) {
                $menuItemPath = null;
                if ($request->attributes->get('use_user_type_for_the_route')) {
                    $menuItemPath = $request->user_space . (current($menuItem) !== 'home' ? ('/' . current($menuItem)) : '');
                } else {
                    $menuItemPath = (current($menuItem) !== 'home' ? ('' . current($menuItem)) : '/');
                }
                if (!is_null($menuItemPath) && $request->is($menuItemPath)) {
                    $currentMenuItem = current($menuItem);
                    break;
                }
            }
        }

        $view = view('artlook::user_space.shared.main_menu');
        $view->with('current_menu_item', $currentMenuItem);

        if (is_array($parameters) && !empty($parameters)) {

            if (isset($parameters['nav_classes']) && !empty($parameters['nav_classes'])) {
                if (is_array($parameters['nav_classes'])) {
                    $view->with('nav_classes', $parameters['nav_classes']);
                } elseif (strlen(strval($parameters['nav_classes'])) > 0) {
                    $view->with('nav_classes', explode(' ', $parameters['nav_classes']));
                }
            }
        }

        return $view;
    }

    public function getCarousel(Request $request, $item, $imageSize, $carouselId = null)
    {
        $allImages = [];
        $largeImages = [];
        $viewHelper = new ViewHelper();
        $settings = $request->attributes->get('settings');


        if (!isset($item->all_images) || empty($item->all_images)) {
            return '';
        }

        $view = view('artlook::user_space.shared.carousel')
            ->with('carousel_item', $item)
            ->with('carousel_id', (!is_null($carouselId) ? $carouselId : uniqid()));

        foreach ($item->all_images as $image) {
            $largeImages[] = $viewHelper->getAPICacheImagePath($image, $settings->tenant_id, 'original');
            $allImages[] = $viewHelper->getAPICacheImagePath($image, $settings->tenant_id, $imageSize);
        }

        $route = Route::current();
        $routeName = $route->getName();
        if (preg_match('/_item$/is', $routeName)) {
            $view->with('add_anchor_image_to_item', true);
        }

        return $view
            ->with('carousel_images', $allImages)
            ->with('large_images', $largeImages);
    }

	public function getHeadMetas( Request $request ) {
		$settings = $request->attributes->get( 'settings' );

		if ( ! isset( $settings->meta ) || empty( (array) $settings->meta ) ) {
			return;
		}

		$view = view( 'artlook::user_space.shared.head_metas' );

		return $view
			->with( 'metas', (array) $settings->meta );
	}
}
