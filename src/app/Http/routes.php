<?php

/*
|--------------------------------------------------------------------------
| Mockups Routes
|--------------------------------------------------------------------------
*/
Route::group([
    'prefix' => 'mockups',
    'namespace' => 'Artlook\Frontend\Http\Controllers'
], function () {
    Route::get('/{template}', [
        'as' => 'mockups_template',
        'uses' => 'MockupsController@template'
    ]);
});

/*
|--------------------------------------------------------------------------
| Shared User Spaces Routes
|--------------------------------------------------------------------------
*/

$userSpaceSharedRoutes = function () {
    Route::get('/exhibition/{exhibition_id}/{exhibition_slug}/artists/{artist_slug}/{artist_id}/{item_slug}/{item_id}', [
        'as' => 'user_space_exhibition_artist_item',
        'uses' => 'UserSpaceController@exhibitionArtistItem'
    ])
         ->where('exhibition_id', '[0-9]+')
         ->where('artist_id', '[0-9]+')
         ->where('item_id', '[0-9]+');
    Route::get('/gallery/{gallery_id}/{gallery_slug}/artists/{artist_slug}/{artist_id}/{item_slug}/{item_id}', [
        'as' => 'user_space_gallery_artist_item',
        'uses' => 'UserSpaceController@galleryArtistItem'
    ])
         ->where('gallery_id', '[0-9]+')
         ->where('artist_id', '[0-9]+')
         ->where('item_id', '[0-9]+');
    Route::get('/pages/{page_id}/{page_slug}/artists/{artist_slug}/{artist_id}/{item_slug}/{item_id}', [
        'as' => 'user_space_page_artist_item',
        'uses' => 'UserSpaceController@pageArtistItem'
    ])
         ->where('page_id', '[0-9]+')
         ->where('artist_id', '[0-9]+')
         ->where('item_id', '[0-9]+');
    Route::get('/exhibition/{exhibition_id}/{exhibition_slug}/artists/{artist_slug}/{artist_id}', [
        'as' => 'user_space_exhibition_artist',
        'uses' => 'UserSpaceController@exhibitionArtist'
    ])
        ->where('exhibition_id', '[0-9]+')
        ->where('artist_id', '[0-9]+');
    Route::get('/gallery/{gallery_id}/{gallery_slug}/artists/{artist_slug}/{artist_id}', [
        'as' => 'user_space_gallery_artist',
        'uses' => 'UserSpaceController@galleryArtist'
    ])
        ->where('gallery_id', '[0-9]+')
        ->where('artist_id', '[0-9]+');
    Route::get('/pages/{page_id}/{page_slug}/artists/{artist_slug}/{artist_id}', [
        'as' => 'user_space_page_artist',
        'uses' => 'UserSpaceController@pageArtist'
    ])
         ->where('page_id', '[0-9]+')
         ->where('artist_id', '[0-9]+');
    Route::get('/exhibition/{exhibition_id}/{item_id}/{item_slug}', [
        'as' => 'user_space_exhibition_item',
        'uses' => 'UserSpaceController@exhibitionItem'
    ])
        ->where('exhibition_id', '[0-9]+')
        ->where('item_id', '[0-9]+');
    Route::get('/gallery/{gallery_id}/{item_id}/{item_slug}', [
        'as' => 'user_space_gallery_item',
        'uses' => 'UserSpaceController@galleryItem'
    ])
        ->where('gallery_id', '[0-9]+')
        ->where('item_id', '[0-9]+');
    Route::get('/artist/{artist_id}/{item_id}/{item_slug}', [
        'as' => 'user_space_artist_item',
        'uses' => 'UserSpaceController@artistItem'
    ])
        ->where('gallery_id', '[0-9]+')
        ->where('item_id', '[0-9]+');
    Route::get('/pages/{page_id}/{item_id}/{item_slug}', [
        'as' => 'user_space_pages_item',
        'uses' => 'UserSpaceController@pageItem'
    ])
        ->where('page_id', '[0-9]+')
        ->where('item_id', '[0-9]+');
    Route::get('/exhibition/{id}/{slug}', [
        'as' => 'user_space_exhibition',
        'uses' => 'UserSpaceController@exhibition'
    ])
        ->where('id', '[0-9]+');
    Route::get('/gallery/{id}/{slug}', [
        'as' => 'user_space_gallery',
        'uses' => 'UserSpaceController@gallery'
    ])
        ->where('id', '[0-9]+');
    Route::get('/artist/{id}/{slug}', [
        'as' => 'user_space_artist',
        'uses' => 'UserSpaceController@artist'
    ])
        ->where('id', '[0-9]+');
    Route::get('/pages/{id}/{slug}', [
        'as' => 'user_space_page',
        'uses' => 'UserSpaceController@page'
    ])
        ->where('id', '[0-9]+');
    Route::get('/item/{id}/{slug}', [
        'as' => 'user_space_item',
        'uses' => 'UserSpaceController@item'
    ])
        ->where('id', '[0-9]+');
    Route::get('/biography', [
        'as' => 'user_space_biography',
        'uses' => 'UserSpaceController@biography'
    ]);
    Route::get('/contact', [
        'as' => 'user_space_contact',
        'uses' => 'UserSpaceController@contact'
    ]);
    Route::post('/contact', [
        'as' => 'user_space_contact_form',
        'uses' => 'UserSpaceController@contactForm'
    ]);
    Route::get('/exhibitions', [
        'as' => 'user_space_exhibitions',
        'uses' => 'UserSpaceController@exhibitions'
    ]);
    Route::get('/galleries', [
        'as' => 'user_space_galleries',
        'uses' => 'UserSpaceController@galleries'
    ]);
    Route::get('/artists', [
        'as' => 'user_space_artists',
        'uses' => 'UserSpaceController@artists'
    ]);
    Route::get('/', [
        'as' => 'user_space_index',
        'uses' => 'UserSpaceController@index'
    ]);
};

/*
|--------------------------------------------------------------------------
| Groups User Spaces Routes
|--------------------------------------------------------------------------
*/

Route::group([
    'as' => 'subdomain::',
    'domain' => config('artlook.settings.main_domain'),
    'prefix' => '{user_space}',
    'namespace' => 'Artlook\Frontend\Http\Controllers',
    'middleware' => [
        'Artlook\Frontend\Http\Middleware\RoutesMiddleware',
        'Artlook\Frontend\Http\Middleware\SettingsMiddleware'
    ],
], function () use ($userSpaceSharedRoutes) {
    $userSpaceSharedRoutes();
});

Route::group([
    'as' => 'domain::',
    'namespace' => 'Artlook\Frontend\Http\Controllers',
    'middleware' => [
        'Artlook\Frontend\Http\Middleware\RoutesMiddleware',
        'Artlook\Frontend\Http\Middleware\SettingsMiddleware'
    ],
], function () use ($userSpaceSharedRoutes) {
    $userSpaceSharedRoutes();
});
