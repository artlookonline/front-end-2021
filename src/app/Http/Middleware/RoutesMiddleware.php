<?php

namespace Artlook\Frontend\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RoutesMiddleware
{
    private $userSpaceNamespace = 'Artlook\Frontend\Http\Controllers';
    private $userSpaceController = 'UserSpaceController';
    private $customNamespace = 'App\Http\Controllers';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $routeAction = $request->route()->getAction();

        if (!isset($routeAction['uses'])) {
            next($request);
        }

        $routeActionUses = explode('@', $routeAction['uses']);

        list($routeActionControllerClass, $routeActionControllerAction) = $routeActionUses;

        $routeActionCustomControllerClass = str_replace($this->userSpaceNamespace, $this->customNamespace, $routeActionControllerClass);

        if (class_exists($routeActionCustomControllerClass)
            && method_exists($routeActionCustomControllerClass, $routeActionControllerAction)
        ) {
            $customRouteAction = array(
                'uses' => str_replace($this->userSpaceNamespace, $this->customNamespace, $routeAction['uses'])
            );

            if (isset($routeAction['controller'])) {
                $customRouteAction['controller'] = str_replace($this->userSpaceNamespace, $this->customNamespace, $routeAction['controller']);
            }

            if (isset($routeAction['namespace'])) {
                $customRouteAction['namespace'] = str_replace($this->userSpaceNamespace, $this->customNamespace, $routeAction['namespace']);
            }

            $routeAction = array_merge($routeAction, $customRouteAction);

            $request->route()->setAction($routeAction);
        }

        return $next($request);
    }
}
