<?php

namespace Artlook\Frontend;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Illuminate\Support\Facades\Storage;

/**
 * ToDo@ZCL2017017
 *
 * · Probar a usar las clases de Illuminate\Support\ siempre que sea posible.
 * · Probar el envío de formularios.
 * · Probar el funcionamiento con un dominio de primer nivel.
 *
 */

/**
 * Class ServiceProvider
 * @package Artlook\Frontend
 */
class ServiceProvider extends BaseServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/artlook.php', 'artlook'
        );
    }

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Register Artlook
         * routes.
         */
        if (!$this->app->routesAreCached()) {
            require __DIR__ . '/Http/routes.php';
        }

        /**
         * Load Artlook
         * views.
         */
        $this->loadViewsFrom(resource_path('views'), 'artlook');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'artlook');

        /**
         * Register Artlook
         * services providers.
         */
        $this->app->register('Artlook\Frontend\Providers\ApiBridgeHelperServiceProvider');
        $this->app->register('Artlook\Frontend\Providers\ViewHelperServiceProvider');
        $this->app->register('Artlook\Frontend\Providers\RouteHelperServiceProvider');

        /**
         * Register Debugbar
         * in controller environments.
         */
        $allowedDebugIps = explode(',', env('APP_ALLOWED_DEBUG_IPS'));

        if ($this->app->environment('local')
            || (
                is_array($allowedDebugIps)
                && !empty($allowedDebugIps)
                && in_array(request()->ip(), $allowedDebugIps))
        ) {
            $this->app->register('Barryvdh\Debugbar\ServiceProvider');
        }

        /**
         * Load Artlook
         * facades.
         */
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('ApiBridgeHelper', 'Artlook\Frontend\Facades\ApiBridgeHelperFacade');
        $loader->alias('RouteHelper', 'Artlook\Frontend\Facades\RouteHelperFacade');
        $loader->alias('ViewHelper', 'Artlook\Frontend\Facades\ViewHelperFacade');

        /**
         * Publish Artlook
         * resources.
         */
        $miscPublishes = [
            __DIR__ . '/../.env.artlook.example' => base_path('.env.artlook.example'),
            __DIR__ . '/../package.json' => base_path('package.json'),
            __DIR__ . '/../.bowerrc' => base_path('.bowerrc'),
            __DIR__ . '/../gulpfile.js.example' => base_path('gulpfile.js.example'),
        ];

        /**
         * ToDo
         *
         * ZCL@20170821
         *
         * Fijar este comportamiento,
         * ahora no hace la copia correctamente.
         */
        if (!file_exists(base_path('gulpfile.js'))) {
            Storage::copy(__DIR__ . '/../gulpfile.js.example', __DIR__ . '/../gulpfile.js');
            $miscPublishes[__DIR__ . '/../gulpfile.js'] = base_path('gulpfile.js');
        }

        if (!file_exists(base_path('bower.json'))) {
            $miscPublishes[__DIR__ . '/../bower.json'] = base_path('bower.json');
        }

        $this->publishes($miscPublishes, 'misc');

        $this->publishes([
            __DIR__ . '/../resources/assets' => resource_path('assets')
        ], 'assets');

        $publicPublishes = [
            __DIR__ . '/../public/user_space' => public_path('user_space'),
            __DIR__ . '/../public/build/user_space' => public_path('build/user_space'),
        ];

        if (!is_dir(public_path('build/fonts'))) {
            $publicPublishes[__DIR__ . '/../public/build/fonts'] = public_path('build/fonts');
        }

        if (!is_dir(public_path('build/img'))) {
            $publicPublishes[__DIR__ . '/../public/build/img'] = public_path('build/img');
        }

        if (!file_exists(public_path('.htaccess'))) {
            $publicPublishes[__DIR__ . '/../public/.htaccess'] = public_path('.htaccess');
        }

        if (!file_exists(public_path('build/rev-manifest.json'))) {
            $publicPublishes[__DIR__ . '/../public/build/rev-manifest.json'] = public_path('build/rev-manifest.json');
        }

        $this->publishes($publicPublishes, 'public');

        /**
         * Load Artlook
         * translations.
         */
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'artlook');
    }
}
