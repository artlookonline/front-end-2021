<?php

namespace Artlook\Frontend\Facades;

use Illuminate\Support\Facades\Facade;

class ViewHelperFacade extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'helper.view';
    }
}