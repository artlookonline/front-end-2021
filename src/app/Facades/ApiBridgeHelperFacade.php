<?php

namespace Artlook\Frontend\Facades;

use Illuminate\Support\Facades\Facade;

class ApiBridgeHelperFacade extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'helper.api_bridge';
    }
}