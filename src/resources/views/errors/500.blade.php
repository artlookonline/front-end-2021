@extends('layouts.errors')

@section('title', 'Internal Server Error')

@section('content')
    <div class="title">
        {{ $exception->getStatusCode() }}
    </div>
@endsection