@extends('layouts.errors')

@section('title', 'Not Found')

@section('content')
    <div class="title">
        {{ $exception->getStatusCode() }}
    </div>
@endsection