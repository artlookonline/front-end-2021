<!DOCTYPE html>
<html lang="en">
<head>
    <!-- MODIFIED JH -->
    @if($settings->web_fonts == 6)
        <link href='//fonts.googleapis.com/css?family=Satisfy|Roboto:400,400italic,700,700italic'
              rel='stylesheet'
              type='text/css'>
    @elseif($settings->web_fonts == 5)
        <link href='//fonts.googleapis.com/css?family=Cabin:400,700|Inconsolata:400,700'
              rel='stylesheet'
              type='text/css'>
    @elseif($settings->web_fonts == 4)
        <link href='//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i|Vollkorn:400,700'
              rel='stylesheet'
              type='text/css'>
    @elseif($settings->web_fonts == 3)
        <link href='//fonts.googleapis.com/css?family=Lato:400,400i,900,900i|Montserrat:400,700'
              rel='stylesheet'
              type='text/css'>
    @elseif($settings->web_fonts == 2)
        <link href='//fonts.googleapis.com/css?family=Muli:300,400|Playfair+Display:400,400i,900,900i'
              rel='stylesheet'
              type='text/css'>
    @else
        <link href='//fonts.googleapis.com/css?family=Fauna+One|Playfair+Display:400,400i,900,900i'
              rel='stylesheet'
              type='text/css'>
    @endif
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @if (isset($settings->meta)
        and !empty((array)$settings->meta))
        @inject('viewController', 'Artlook\Frontend\Http\Controllers\ViewController')
        {!! $viewController->getHeadMetas(app('request')) !!}
    @endif
    <title>@yield('title') - {{ $settings->orgname }}</title>

    <!---- snipcart ---->
    @if(isset($settings->web_snipcart))
    <link rel="preconnect" href="https://app.snipcart.com">
    <link rel="preconnect" href="https://cdn.snipcart.com">

    <link rel="stylesheet" href="https://cdn.snipcart.com/themes/v3.2.1/default/snipcart.css"/>
    @endif
    <!--- snipcart ends --->


    <meta name="title" content="@yield('title') - {{ $settings->orgname }}"/>
    @if (isset($settings->web_descript))
        <meta name="description" content="{{ $settings->web_descript }}"/>
    @endif
    @if (isset($settings->web_key))
        <meta name="keywords" content="{{ $settings->web_key }}"/>
    @endif
    <!-- OG -> -->
    @section('head_og_site_name')
        <meta property="og:site_name" content="{{ $settings->orgname }}"/>
    @show
    @section('head_og_url')
        <meta property="og:url" content="{{ Request::fullUrl() }}"/>
    @show
    @section('head_og_title')
        <meta property="og:title" content="@yield('title') - {{ $settings->orgname }}"/>
    @show
    @section('head_og_description')
        @if (isset($settings->web_descript))
            <meta property="og:description" content="{{ $settings->web_descript }}"/>
        @endif
    @show
    @yield('head_og_type')
    @yield('head_og_image')

    <!-- <- OG  -->
    <link href="{{ elixir('user_space/css/all.css') }}" rel="stylesheet">

    @if (isset($settings->css) and !empty($settings->css))
        <style type="text/css">
            {!! $settings->css !!}
        </style>
    @endif

    @if(isset($settings->web_snipcart))
        <style>
            .snipcart-order__footer {
                position: relative !important;
            }
        </style>

    @endif

    <!-- for youtube embedded iframe -->
    <style>
        .embed-container {
            position: relative;
            padding-bottom: 56.25%;
            overflow: hidden;
            max-width: 100%;
            height: auto;
        }

        .embed-container iframe,
        .embed-container object,
        .embed-container embed {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
    </style>
    <!-- youtube end-->

    @yield('styles')  {{--    pick up any page styles --}}


    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('recaptcha_script')


{{-- Check if Analytics 4--}}
    @if(isset($settings->analytics) and !empty($settings->analytics) && substr($settings->analytics, 0, 1) === 'G')
        @include('artlook::user_space.shared.analytics_tracking_4')
    @else
        @include('artlook::user_space.shared.analytics_tracking')
    @endif



</head>
<body class="theme-{{ (isset($settings->web_template) and intval($settings->web_template) > 1) ? $settings->web_template : '1' }} typography-pack-{{ (isset($settings->web_fonts) and intval($settings->web_fonts) > 1) ? $settings->web_fonts : '1' }} color-pack-{{ (isset($settings->web_colours) and intval($settings->web_colours) > 1) ? $settings->web_colours : '1' }} @stack('body_class')">
@include('artlook::user_space.blocks.theme_'.((isset($settings->web_template) and intval($settings->web_template) > 1) ? $settings->web_template : '1').'_header')
@include('artlook::user_space.blocks.theme_'.((isset($settings->web_template) and intval($settings->web_template) > 1) ? $settings->web_template : '1').'_main')
@include('artlook::user_space.blocks.theme_'.((isset($settings->web_template) and intval($settings->web_template) > 1) ? $settings->web_template : '1').'_footer')

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ elixir('user_space/js/jquery.js') }}"><\/script>')</script>
<script src="{{ elixir('user_space/js/vendors.js') }}"></script>
<script src="{{ elixir('user_space/js/app.js') }}"></script>
<script src="https://use.fontawesome.com/f5f8e0e63d.js"></script>

<!---- snipcart ---->
@if(isset($settings->web_snipcart))
<script async src="https://cdn.snipcart.com/themes/v3.2.1/default/snipcart.js"></script>
@endif
<!---- snipcart ends ---->

@yield('scripts')
</body>
</html>
