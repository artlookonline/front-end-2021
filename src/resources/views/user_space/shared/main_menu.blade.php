<ul class="nav navbar-nav {{ isset($nav_classes) && !empty($nav_classes) ? implode(' ', $nav_classes) : '' }}">
    @foreach ($settings->menu as $menuItem)
        <li class="{{ $current_menu_item === current($menuItem) ? 'active' : '' }}">
            @if($use_user_type_for_the_route)
                <a href="/{{ app('request')->user_space }}{{ current($menuItem) !== 'home' ? ('/'.current($menuItem)) : '' }}">{{key($menuItem)}}</a>
            @else
                <a href="/{{ current($menuItem) !== 'home' ? (current($menuItem)) : '' }}">{{key($menuItem)}}</a>
            @endif
        </li>
    @endforeach
    <li class="{{ (app('request')->route()->getName()) === $domain_type.'::user_space_contact' ? 'active' : '' }}">
        <a href="{{ RouteHelper::route('user_space_contact', [ 'user_space' => app('request')->user_space ]) }}">@lang('artlook::artlook.Contact')</a>
    </li>
</ul>