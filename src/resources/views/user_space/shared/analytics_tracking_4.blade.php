<!-- Global site tag (gtag.js) - Google Analytics -->
{{--<script async src="https://www.googletagmanager.com/gtag/js?id=G-P73L75PN4W"></script>--}}
<script async src="https://www.googletagmanager.com/gtag/js?id={{ $settings->analytics }}"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', '{{ $settings->analytics }}');
</script>