@if((isset($settings->web_facebook) and !empty($settings->web_facebook))
or (isset($settings->web_twitter) and !empty($settings->web_twitter))
or (isset($settings->web_pinterest) and !empty($settings->web_pinterest))
or (isset($settings->web_instagram) and !empty($settings->web_instagram)))
    <ul style="display: inline-block">
        @if(isset($settings->web_facebook) and !empty($settings->web_facebook))
            <li><a href="{{ $settings->web_facebook }}" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        @endif
        @if(isset($settings->web_twitter) and !empty($settings->web_twitter))
            <li><a href="{{ $settings->web_twitter }}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        @endif
        @if(isset($settings->web_pinterest) and !empty($settings->web_pinterest))
            <li><a href="{{ $settings->web_pinterest }}" target="_blank"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
        @endif
        @if(isset($settings->web_instagram) and !empty($settings->web_instagram))
            <li><a href="{{ $settings->web_instagram }}" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
        @endif
    </ul>
@endif