@extends('artlook::user_space.layouts.default')

@inject('viewController', 'Artlook\Frontend\Http\Controllers\ViewController')

@push('body_class', ' gallery ')

@php
    $masonryClasses = ['container' => '', 'item' => ''];
@endphp

@if (isset($settings->web_template)
and (
intval($settings->web_template) === 2
or intval($settings->web_template) === 3
))
    @push('body_class', ' masonry ')
    @php
        $masonryClasses = ['container' => 'masonry-container', 'item' => 'masonry-item'];
    @endphp
@endif

@section('title', isset($title) ? $title : '')

@section('main')
    @if ((isset($main_item) and !empty($main_item))
        or (isset($subsidiaries_items) and !empty($subsidiaries_items)))
        <!-- GROUPING LAYOUT//////////////////////////////////////////////////////   -->
        <section class="grouping grouping-layout grouping-layout-gallery-detail">
            <div class="container">
                <article class="row">
                    <div class="grouping-layout-title">
                        <h1 title="{{ htmlspecialchars($title) }}">{{ $title }}</h1>
                    </div>
                    @if(isset($main_item->description) and !empty($main_item->description))
                        <div class="grouping-content">
                            <div class="grouping-content-text">
                                {!! $main_item->description !!}
                            </div>
                        </div>
                    @endif
                    @if (isset($main_item->all_images) and !empty($main_item->all_images))
                        <!-- <div class="grouping-image">
                            {!! $viewController->getCarousel(app('request'), $main_item, 'w768', 'galleryMainCarousel') !!}
                        </div>-->
                    @endif
                    @if ((isset($main_item->items) and !empty($main_item->items)))
                        <div class="grouping-gallery">
                            <ul class="row {{ $masonryClasses['container'] }}">
                                @if (isset($main_item->artist_indexed) and $main_item->artist_indexed === 1
                                and isset($main_item->artists) and !empty($main_item->artists))
                                    @foreach ($main_item->artists as $artist_id => $artist_name)
                                        <li class="{{ $masonryClasses['item'] }}">
                                            <a href="{{ RouteHelper::route('user_space_gallery_artist', [ 'user_space' => app('request')->user_space, 'gallery_id' => $main_item->id, 'gallery_slug' => ViewHelper::getSlug($main_item->title), 'artist_slug' => ViewHelper::getSlug($artist_name), 'artist_id' => $artist_id ]) }}">
                                                <figure>
                                                    <span>
                                                        <img src="{{ ViewHelper::getArtistAPICacheImage($artist_id, $settings->tenant_id, 'w768') }}">
                                                    </span>
                                                    <figcaption>
                                                        {{ $artist_name }}
                                                    </figcaption>
                                                </figure>
                                            </a>
                                        </li>
                                    @endforeach
                                @else
                                    @foreach ($main_item->items as $currentItem)
                                        <li class="{{ $masonryClasses['item'] }}">
                                            <a href="{{ RouteHelper::route('user_space_gallery_item', [ 'user_space' => app('request')->user_space, 'gallery_id' => $main_item->id, 'item_id' => $currentItem->id, 'item_slug' => ViewHelper::getSlug($currentItem->title) ]) }}">
                                                <figure>
                                                    <span>
                                                        <img src="{{ ViewHelper::getAPICacheImagePath($currentItem->image, $settings->tenant_id, 'w768') }}"
                                                             alt="{{ $currentItem->title }} - {{ $currentItem->artist }}">
                                                    </span>
                                                    <figcaption>@if($settings->type === 'gallery')<small class="item-artist-name">{{ $currentItem->artist }}</small>@endif{{ $currentItem->title }}
                                                    <br>

                                                        @include('artlook::user_space.shared.thumb_status_display')


                                                    </figcaption>
                                                </figure>


                                            </a>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    @endif
                </article>
            </div>
        </section>
        <!-- END GROUPING //////////////////////////////////////////////////////   -->
    @endif
@endsection
