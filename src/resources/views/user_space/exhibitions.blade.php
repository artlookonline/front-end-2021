@extends('artlook::user_space.layouts.default')

@inject('viewController', 'Artlook\Frontend\Http\Controllers\ViewController')

@push('body_class', ' exhibitions ')

@php
    $masonryClasses = ['container' => '', 'item' => ''];
@endphp

@if (isset($settings->web_template)
and (
intval($settings->web_template) === 2
or intval($settings->web_template) === 3
))
    @push('body_class', ' masonry ')
    @php
        $masonryClasses = ['container' => 'masonry-container', 'item' => 'masonry-item'];
    @endphp
@endif

@section('title', isset($title) ? $title : '')

@section('main')
    <!-- GROUPING LAYOUT//////////////////////////////////////////////////////   -->
    @if (isset($main_item) and !is_null($main_item))
        <section class="grouping grouping-layout grouping-layout-exhibition grouping-first">
            <div class="container">
                <article class="row {{ (isset($main_item->grouping_classes) and !empty($main_item->grouping_classes)) ? (implode(' ', array_filter($main_item->grouping_classes))) : '' }}">
                    <div class="grouping-image">
                        @if (isset($main_item->all_images) and !empty($main_item->all_images))
                            <a href="{{ RouteHelper::route('user_space_exhibition', [ 'user_space' => app('request')->user_space, 'id' => $main_item->id, 'slug' => ViewHelper::getSlug((isset($main_item->full_name) ? $main_item->full_name : $main_item->title)) ]) }}">{!! $viewController->getCarousel(app('request'), $main_item, 'x768', 'exhibitionMainCarousel') !!}</a>
                        @endif
                    </div>
                    <div class="grouping-content">
                        @if ((isset($main_item->dateStart) and !empty($main_item->dateStart))
                            or (isset($main_item->dateEnd) and !empty($main_item->dateEnd))
                            or (isset($main_item->location) and !empty($main_item->location)))
                            <div class="grouping-content-data">
                                <ul>
                                    @if ((isset($main_item->dateStart) and !empty($main_item->dateStart))
                                        or (isset($main_item->dateEnd) and !empty($main_item->dateEnd)))
                                        <li class="grouping-content-data-data">{{ (isset($main_item->dateStart) and !empty($main_item->dateStart)) ? $main_item->dateStart : '' }} {{ (isset($main_item->dateEnd) and !empty($main_item->dateEnd)) ? trans('artlook::artlook.to') . $main_item->dateEnd : '' }}</li>
                                    @endif
                                    @if (isset($main_item->location) and !empty($main_item->location))
                                        <li class="grouping-content-data-location">{{ strip_tags($main_item->location) }}</li>
                                    @endif
                                </ul>
                            </div>
                        @endif
                        <div class="grouping-content-title">
                            <h1>
                                <a href="{{ RouteHelper::route('user_space_exhibition', [ 'user_space' => app('request')->user_space, 'id' => $main_item->id, 'slug' => ViewHelper::getSlug((isset($main_item->full_name) ? $main_item->full_name : $main_item->title)) ]) }}">{{ isset($main_item->full_name) ? $main_item->full_name : $main_item->title }}</a>
                            </h1>
                        </div>
                        @if(isset($main_item->description) and !empty($main_item->description))
                            <div class="grouping-content-text">
                                {!! $main_item->description !!}
                            </div>
                        @endif
                        <div class="grouping-content-link">
                            <a href="{{ RouteHelper::route('user_space_exhibition', [ 'user_space' => app('request')->user_space, 'id' => $main_item->id, 'slug' => ViewHelper::getSlug((isset($main_item->full_name) ? $main_item->full_name : $main_item->title)) ]) }}"
                               class="btn btn-transparent btn-arrow"><i class="fa fa-long-arrow-right"
                                                                        aria-hidden="true"></i></a>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    @endif
    @if (isset($subsidiaries_items) and !empty($subsidiaries_items))
        <section class="grouping grouping-layout grouping-layout-exhibition grouping-list">
            <div class="container">
                <div class="row">
                    @foreach ($subsidiaries_items as $subsidiariesItem)
                        <article class="{{ (isset($subsidiariesItem->grouping_classes) and !empty($subsidiariesItem->grouping_classes)) ? (implode(' ', array_filter($subsidiariesItem->grouping_classes))) : '' }}">
                                <div class="grouping-image">
                                    @if (isset($subsidiariesItem->all_images) and !empty($subsidiariesItem->all_images))
                                        <a href="{{ RouteHelper::route('user_space_exhibition', [ 'user_space' => app('request')->user_space, 'id' => $subsidiariesItem->id, 'slug' => ViewHelper::getSlug((isset($subsidiariesItem->full_name) ? $subsidiariesItem->full_name : $subsidiariesItem->title)) ]) }}">{!! $viewController->getCarousel(app('request'), $subsidiariesItem, 'w768', $subsidiariesItem->id.'Carousel') !!}</a>
                                    @endif
                                </div>
                            <div class="grouping-content">
                                @if ((isset($subsidiariesItem->dateStart) and !empty($subsidiariesItem->dateStart))
                                    or (isset($subsidiariesItem->dateEnd) and !empty($subsidiariesItem->dateEnd))
                                    or (isset($subsidiariesItem->location) and !empty($subsidiariesItem->location)))
                                    <div class="grouping-content-data">
                                        <ul>
                                            @if ((isset($subsidiariesItem->dateStart) and !empty($subsidiariesItem->dateStart))
                                                or (isset($subsidiariesItem->dateEnd) and !empty($subsidiariesItem->dateEnd)))
                                                <li class="grouping-content-data-data">{{ (isset($subsidiariesItem->dateStart) and !empty($subsidiariesItem->dateStart)) ? $subsidiariesItem->dateStart : '' }} {{ (isset($subsidiariesItem->dateEnd) and !empty($subsidiariesItem->dateEnd)) ? trans('artlook::artlook.to') . $subsidiariesItem->dateEnd : '' }}</li>
                                            @endif
                                            @if (isset($subsidiariesItem->location) and !empty($subsidiariesItem->location))
                                                <li class="grouping-content-data-location">{{ strip_tags($subsidiariesItem->location) }}</li>
                                            @endif
                                        </ul>
                                    </div>
                                @endif
                                <div class="grouping-content-title">
                                    <h1>
                                        <a href="{{ RouteHelper::route('user_space_exhibition', [ 'user_space' => app('request')->user_space, 'id' => $subsidiariesItem->id, 'slug' => ViewHelper::getSlug((isset($subsidiariesItem->full_name) ? $subsidiariesItem->full_name : $subsidiariesItem->title)) ]) }}">{{ isset($subsidiariesItem->full_name) ? $subsidiariesItem->full_name : $subsidiariesItem->title }}</a>
                                    </h1>
                                </div>
                                @if(isset($subsidiariesItem->description) and !empty($subsidiariesItem->description))
                                    <div class="grouping-content-text">
                                        {!! ViewHelper::truncateHtml($subsidiariesItem->description, 600) !!}
                                    </div>
                                @endif
                                <div class="grouping-content-link">
                                    <a href="{{ RouteHelper::route('user_space_exhibition', [ 'user_space' => app('request')->user_space, 'id' => $subsidiariesItem->id, 'slug' => ViewHelper::getSlug((isset($subsidiariesItem->full_name) ? $subsidiariesItem->full_name : $subsidiariesItem->title)) ]) }}"
                                       class="btn btn-transparent btn-arrow"><i class="fa fa-long-arrow-right"
                                                                                aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </article>
                    @endforeach
                </div>
            </div>
        </section>
    @endif
    <!-- END GROUPING //////////////////////////////////////////////////////   -->
@endsection
