@extends('artlook::user_space.layouts.default')

@inject('viewController', 'Artlook\Frontend\Http\Controllers\ViewController')

@push('body_class', ' gallery artist')

@php
    $masonryClasses = ['container' => '', 'item' => ''];
@endphp

@if (isset($settings->web_template)
and (
intval($settings->web_template) === 2
or intval($settings->web_template) === 3
))
    @push('body_class', ' masonry ')
    @php
        $masonryClasses = ['container' => 'masonry-container', 'item' => 'masonry-item'];
    @endphp
@endif

@section('title', isset($title) ? $title : '')

@section('main')
    @if ((isset($main_item) and !empty($main_item))
        or (isset($subsidiaries_items) and !empty($subsidiaries_items)))
        <!-- GROUPING LAYOUT//////////////////////////////////////////////////////   -->
        <section class="grouping grouping-layout grouping-layout-gallery-detail">
            <div class="container">
                <article class="row">
                    <div class="grouping-layout-title">
                        @php
                            $allLinkRouteName = 'user_space_artists';
                            $allLinkRouteParams = [ 'user_space' => app('request')->user_space ];
                            $allLinkLabel = 'All Artists';
                        @endphp
                        @if (isset($parent) and !empty($parent))
                            @if (Route::currentRouteName() === $domain_type.'::user_space_page_artist')
                                @php
                                    $allLinkRouteName = 'user_space_page';
                                    $allLinkRouteParams['id'] = $parent->id;
                                    $allLinkRouteParams['slug'] = ViewHelper::getSlug($parent->title);
                                @endphp
                            @elseif (Route::currentRouteName() === $domain_type.'::user_space_gallery_artist')
                                @php
                                    $allLinkRouteName = 'user_space_gallery';
                                    $allLinkRouteParams['id'] = $parent->id;
                                    $allLinkRouteParams['slug'] = ViewHelper::getSlug($parent->title);
                                @endphp
                            @elseif (Route::currentRouteName() === $domain_type.'::user_space_exhibition_artist')
                                @php
                                    $allLinkRouteName = 'user_space_exhibition';
                                    $allLinkRouteParams['id'] = $parent->id;
                                    $allLinkRouteParams['slug'] = ViewHelper::getSlug($parent->title);
                                @endphp
                            @endif
                            @php
                                $allLinkLabel = $parent->title;
                            @endphp
                        @endif
                        <small class="all-link"><a href="{{ RouteHelper::route($allLinkRouteName, $allLinkRouteParams) }}">{{ $allLinkLabel }}</a></small>
                        <h1 title="{{ htmlspecialchars($title) }}">{{ $title }}</h1>
                    </div>
                    @if (isset($main_item->all_images) and !empty($main_item->all_images))
                        <div class="grouping-image">
                            {!! $viewController->getCarousel(app('request'), $main_item, 'w768', 'galleryMainCarousel') !!}
                        </div>
                    @endif
                    @if((isset($main_item->biography) and !empty($main_item->biography))
                    or (isset($main_item->artistData->short_description) and !empty($main_item->artistData->short_description)))
                        <div class="grouping-content">
                            <div class="grouping-content-text">
                                @if(isset($main_item->biography) and !empty($main_item->biography))
                                    {!! $main_item->biography !!}
                                @elseif(isset($main_item->artistData->short_description) and !empty($main_item->artistData->short_description))
                                    {!! $main_item->artistData->short_description !!}
                                @endif
                            </div>
                        </div>
                    @endif
                    @if ((isset($main_item->works) and !empty($main_item->works)))
                        <div class="grouping-gallery">
                            <ul class="row {{ $masonryClasses['container'] }}">
                                @foreach ($main_item->works as $currentItem)
                                    @php
                                        $itemRouteName = 'user_space_artist_item';
                                        $itemRouteParams = [ 'user_space' => app('request')->user_space, 'artist_id' => $main_item->id, 'item_id' => $currentItem->id, 'item_slug' => ViewHelper::getSlug($currentItem->title) ];
                                    @endphp
                                    @if (isset($parent) and !empty($parent))
                                        @if (Route::currentRouteName() === $domain_type.'::user_space_page_artist')
                                            @php
                                                $itemRouteName = 'user_space_page_artist_item';
                                                $itemRouteParams['artist_slug'] = ViewHelper::getSlug($title);
                                                $itemRouteParams['page_id'] = $parent->id;
                                                $itemRouteParams['page_slug'] = ViewHelper::getSlug($parent->title);
                                            @endphp
                                        @elseif (Route::currentRouteName() === $domain_type.'::user_space_gallery_artist')
                                            @php
                                                $itemRouteName = 'user_space_gallery_artist_item';
                                                $itemRouteParams['artist_slug'] = ViewHelper::getSlug($title);
                                                $itemRouteParams['gallery_id'] = $parent->id;
                                                $itemRouteParams['gallery_slug'] = ViewHelper::getSlug($parent->title);
                                            @endphp
                                        @elseif (Route::currentRouteName() === $domain_type.'::user_space_exhibition_artist')
                                            @php
                                                $itemRouteName = 'user_space_exhibition_artist_item';
                                                $itemRouteParams['artist_slug'] = ViewHelper::getSlug($title);
                                                $itemRouteParams['exhibition_id'] = $parent->id;
                                                $itemRouteParams['exhibition_slug'] = ViewHelper::getSlug($parent->title);
                                            @endphp
                                        @endif
                                    @endif
                                    <li class="{{ $masonryClasses['item'] }}">
                                        <a href="{{ RouteHelper::route($itemRouteName, $itemRouteParams) }}">
                                            <figure>
                                                <span>
                                                    @if(isset($currentItem->image) and !empty($currentItem->image))
                                                        <img src="{{ ViewHelper::getAPICacheImagePath($currentItem->image, $settings->tenant_id, 'w768') }}"
                                                             alt="{{ $title }} - {{ $currentItem->title }}">
                                                    @endif
                                                </span>
                                                <figcaption>
                                                    {{ $currentItem->title }}
                                                    <br>
                                                    @include('artlook::user_space.shared.thumb_status_display')
                                                </figcaption>
                                            </figure>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </article>
            </div>
        </section>
        <!-- END GROUPING //////////////////////////////////////////////////////   -->
    @endif
@endsection
