@extends('artlook::mockups.t3-00-layout_default')

@section('title', 'gallery_detail_c')

@section('main')

    <!-- GROUPING LAYOUT//////////////////////////////////////////////////////   -->
    <section class="grouping grouping-layout grouping-layout-gallery-detail">
        <div class="container">
            <article class="row">
                <div class="grouping-layout-title">
                    <h1><a href="#">Available galleries of work</a></h1>
                </div>
                <div class="grouping-image">
                    <img src="http://placehold.it/768x1024">
                </div>
                <div class="grouping-gallery">
                    <ul class="row">
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x300"></span>
                                    <figcaption>Title</figcaption>
                                </figure>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x440"></span>
                                    <figcaption>Title</figcaption>
                                </figure>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x768"></span>
                                    <figcaption>Title</figcaption>
                                </figure>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x800"></span>
                                    <figcaption>Title</figcaption>
                                </figure>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x250"></span>
                                    <figcaption>Title</figcaption>
                                </figure>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x900"></span>
                                    <figcaption>Title</figcaption>
                                </figure>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x768"></span>
                                    <figcaption>Title</figcaption>
                                </figure>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x768"></span>
                                    <figcaption>Title</figcaption>
                                </figure>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="grouping-content">
                    <div class="grouping-content-text">
<p>Pablo Ruiz y Picasso, also known as Pablo Picasso (was a Spanish painter, sculptor, printmaker, ceramicist, stage designer, poet and playwright who spent most of his adult life in France.</p>

<p>Regarded as one of the greatest and most influential artists of the 20th century, he is known for co-founding the Cubist movement, the invention of constructed sculpture,the co-invention of collage, and for the wide variety of styles that he helped develop and explore. Among his most famous works are the proto-Cubist Les Demoiselles d'Avignon (1907), and Guernica (1937), a portrayal of the Bombing of Guernica by the German and Italian airforces at the behest of the Spanish
nationalist government during the Spanish Civil War.

<p>Picasso, Henri Matisse and Marcel Duchamp are regarded as the three artists who most defined the revolutionary developments in the plastic arts in the opening decades of the 20th century, responsible for
significant developments in painting, sculpture, printmaking and ceramics.

<p>Picasso demonstrated extraordinary artistic talent in his early years, painting in a naturalistic manner through his childhood and adolescence. During the first decade of the 20th century, his style changed as he experimented with different theories, techniques, and ideas. His work is often categorized into periods. While the names of many of his later periods are debated, the most commonly accepted periods in his work are the Blue Period (1901–1904), the Rose Period (1904–1906), the African-influenced Period (1907–1909), Analytic Cubism (1909–1912), and Synthetic Cubism (1912–1919), also referred to as the Crystal period.

<p>Exceptionally prolific throughout the course of his long life, Picasso achieved universal renown and immense fortune for his revolutionary artistic accomplishments, and became one of the best-known figures in 20th-century art.</p>
                    </div>
                </div>
            </article>
        </div>
    </section>
    <!-- END GROUPING //////////////////////////////////////////////////////   -->

@endsection
