@extends('artlook::mockups.t3-00-layout_default')

@section('title', 'exhibitions_c')

@section('main')

    <!-- GROUPING LAYOUT//////////////////////////////////////////////////////   -->
    <section class="grouping grouping-layout grouping-layout-exhibition grouping-first">
        <div class="container">
            <article class="row">
                <div class="grouping-image">
                    <a href="#"><img src="http://placehold.it/768x768"></a>
                </div>
                <div class="grouping-content">
                    <div class="grouping-content-data">
                        <ul>
                            <li class="grouping-content-data-data">1st November 2016 to 31st December 2016</li>
                            <li class="grouping-content-data-location">Name of Location</li>
                        </ul>
                    </div>
                    <div class="grouping-content-title">
                        <h1><a href="#"><a href="#">The portraits</a></h1>
                    </div>
                    <div class="grouping-content-text">
<p>Pablo Ruiz y Picasso, also known as Pablo Picasso (was a Spanish painter, sculptor, printmaker, ceramicist, stage designer, poet and playwright who spent most of his adult life in France.</p>
                    </div>
                    <div class="grouping-content-link">
                        <a href="#" class="btn btn-alt">View details</a>
                    </div>
                </div>
            </article>
        </div>
    </section>

    <section class="grouping grouping-layout grouping-layout-exhibition grouping-list">
        <div class="container">
            <div class="row">
                <article class="grouping-only-text">
                    <div class="grouping-content">
                        <div class="grouping-content-data">
                            <ul>
                                <li class="grouping-content-data-data">1st November 2016 to 31st December 2016</li>
                                <li class="grouping-content-data-location">Name of Location</li>
                            </ul>
                        </div>
                        <div class="grouping-content-title">
                            <h1><a href="#">The portraits</a></h1>
                        </div>
                        <div class="grouping-content-text">
    <p>Pablo Ruiz y Picasso, also known as Pablo Picasso (was a Spanish painter, sculptor, printmaker, ceramicist, stage designer, poet and playwright who spent most of his adult life in France.</p>
                        </div>
                        <div class="grouping-content-link">
                            <a href="#" class="btn btn-transparent btn-arrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </article>

                <article>
                    <div class="grouping-image">
                        <a href="#"><img src="http://placehold.it/768x768"></a>
                    </div>
                    <div class="grouping-content">
                        <div class="grouping-content-data">
                            <ul>
                                <li class="grouping-content-data-data">1st November 2016 to 31st December 2016</li>
                                <li class="grouping-content-data-location">Name of Location</li>
                            </ul>
                        </div>
                        <div class="grouping-content-title">
                            <h1><a href="#">The portraits</a></h1>
                        </div>
                        <div class="grouping-content-text">
    <p>Pablo Ruiz y Picasso, also known as Pablo Picasso (was a Spanish painter, sculptor, printmaker, ceramicist, stage designer, poet and playwright who spent most of his adult life in France.</p>
                        </div>
                        <div class="grouping-content-link">
                            <a href="#" class="btn btn-transparent btn-arrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </section>

    <!-- END GROUPING //////////////////////////////////////////////////////   -->

@endsection
