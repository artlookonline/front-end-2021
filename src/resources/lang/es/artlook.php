<?php

return [
    'toSendUseForm' => 'Para enviar un mensaje usa este formulario',
    'yourName' => 'Tu nombre',
    'yourEmail' => 'Tu email',
    'yourMessage' => 'Tu mensaje',
    'name' => 'Nombre',
    'email' => 'Email',
    'message' => 'Mensaje',
    'phone' => 'Teléfono',
    'subject' => 'Sujecto',
    'enquireAbout' => 'Solicitar sobre ',
    'submit' => 'Enviar',
    'sending' => 'Enviando ...',
    'Contact' => 'Contactar',
    'enquire' => 'Solicitud sobre artículo',
    'paypal' => 'Comprar ahora con Paypal',
	'share' => 'Compartir',
	'to' => 'al ',
	'How to find us or contact us' => 'Como encontrarnos o contactarnos',
];
