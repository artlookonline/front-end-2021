<?php

return [
    'toSendUseForm' => 'To send a message or a request please use this form:',
    'yourName' => 'Your name',
    'yourEmail' => 'Your email',
    'yourMessage' => 'Your message',
    'name' => 'Name',
    'email' => 'Email',
    'message' => 'Message',
    'phone' => 'Phone',
    'subject' => 'Subject',
    'enquireAbout' => 'Enquire about ',
    'submit' => 'Submit',
    'sending' => 'Sending ...',
    'Contact' => 'Contact',
    'enquire' => 'Enquire about this item',
    'paypal' => 'Buy Now with Paypal',
	'share' => 'Share',
	'to' => 'to  ',
	'How to find us or contact us' => 'How to find us or contact us',
];
